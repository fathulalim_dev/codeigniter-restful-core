
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Testing extends REST_Controller {

    public function index_get()
    {
        $this->set_response(array(
            'status'=>'ok',
            'message'=>'api modular is running',
            'data'=>null
        ), REST_Controller::HTTP_OK);
    }

}

/* End of file Testing.php */
